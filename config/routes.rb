Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  mount Facebook::Messenger::Server, at: 'bot'
  # Defines the root path route ("/")
  # root "articles#index"
end
