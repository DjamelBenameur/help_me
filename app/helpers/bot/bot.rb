require 'facebook/messenger'

include Facebook::Messenger
module Bot
  class Bot
    Facebook::Messenger::Subscriptions.subscribe(
      access_token: ENV["ACCESS_TOKEN"],
      subscribed_fields: ['messages', 'messaging_postbacks', 'messaging_optins', 'message_reads']
    )

    Facebook::Messenger::Bot.on :message do |message|
      message.reply(text: 'Hello, human!')
    end
  end
end


